# build
FROM maven:3-jdk-11 as build
COPY . ./boxfuse
WORKDIR /boxfuse
RUN mvn package
#
# run tomcat
FROM tomcat:10.0-jdk11-openjdk
COPY --from=build /boxfuse/target/hello-1.0.war /usr/local/tomcat/webapps/
EXPOSE 8080